

CFLAGS = -Os -Wall -pipe -mtune=mips32 -msoft-float -funit-at-a-time  -fpic -g
CPPFLAGS = -I$(TOOLCHAIN_DIR)/usr/include  -I$(TOOLCHAIN_DIR)/include -I$(SOFTWAREDIR)/include -I$(SOFTWAREDIR)/shared -I$(SOFTWAREDIR)/cfg/include -I$(SOFTWAREDIR)/include/l2

LDFLAGS +=  -lshared -L$(SOFTWAREDIR)/shared 

PLATFORM_LDFLAGS = -lshared 

export CFLAGS  CPPFLAGS LDFLAGS PLATFORM_LDFLAGS
