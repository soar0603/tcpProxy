
PORT_PROXY_DIR = port_proxy

port_proxy-build:
	@$(MAKE) -C $(PORT_PROXY_DIR) \
		CROSS_COMPILE="$(CROSS_COMPILE)" \
		EXTRA_CFLAGS="$(TARGET_CFLAGS)" \
		all
		
port_proxy-install:
	@$(MAKE) -C $(PORT_PROXY_DIR) \
		CROSS_COMPILE="$(CROSS_COMPILE)" \
		EXTRA_CFLAGS="$(TARGET_CFLAGS)" \
		CONFIG_PREFIX="$(TARGETDIR)" \
		install
#	@cp $(PORT_PROXY_DIR)/master /tftpboot

port_proxy-clean:
	@$(MAKE) -C $(PORT_PROXY_DIR) clean
	
