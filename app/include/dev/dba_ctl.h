/*-------------------------------------------------------------------------
    topo.h
-------------------------------------------------------------------------*/

#ifndef DBA_CTL_H_
#define DBA_CTL_H_

extern UINT DBA_CreateGpsDba(UINT8 *szDbaName);
extern UINT DBA_AddGpsDba(UINT8 *szName, TOPO_GPS_INFO_T *stGpsInfo);


#endif /* TOPO_H_ */
