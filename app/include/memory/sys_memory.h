#ifndef __MEM_INFO_H__
#define __MEM_INFO_H__
#include "memory/sys_memshare.h"
#if 1
#define MEM_MAX_NUM  1000 /* 记录最多的内存分配 */
#define MEM_DEBUG_ON        "./mem_debug_on"
#define MEM_DDEBUG_SHMID  0x1213
#define MEM_FunName  64 /* 记录最多的内存分配 */

#define MEM_STRDUP(pucStr)   MEM_Strdup((pucStr), __FUNCTION__, __LINE__)

#define MEM_MALLOC(size)   MEM_Malloc((size), __FUNCTION__, __LINE__)

#define MEM_FREE(pBuf)   MEM_Free((pBuf))

#define MEM_CALLOC(nmemb,size)   MEM_Calloc((nmemb), (size), __FUNCTION__, __LINE__)

#define MEM_REALLOC(ptr,size)   MEM_Realloc((ptr), (size), __FUNCTION__, __LINE__)

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

typedef struct{
    char ucValid;
    char szFunName[MEM_FunName];  /* 函数名 */
    unsigned int uiLine; /* 行号 */
    unsigned int uiIndex;/* 自增数 */
    unsigned int uiSize;/* 分配内存大小 */
    char *pBuf; /* 分配的地址 */
    unsigned int uiPid;/* 进程号 */
}MEM_INFO;
#endif
#endif /*#ifndef __MEM_INFO_H__*/

