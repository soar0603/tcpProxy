
/* ctc_switch_port */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>

#include "private_type.h"
#include "private_log.h"
#include "ipc_protocol.h"
#include "memory/sys_memory.h"


#define VTY_NEWLINE "\r\n"

int MEM_DebugConfig(int Enable)
{
    int ret;
    if (TRUE == Enable)
    {
        MEM_SetDebug(TRUE);
    }
    else
    {
        MEM_SetDebug(FALSE);
    }

    return IPC_STATUS_OK;
}

int MEM_ShowDebugInfo()
{
    int ret = 0;
    int i = 0;
    MEM_INFO astMemDebugInfo[MEM_MAX_NUM];

    memset(astMemDebugInfo, 0, sizeof(astMemDebugInfo));
    ret = MEM_GetDebugInfo(astMemDebugInfo);
    if (ret != 0)
    {
        printf( "mem debug not open!  %s", VTY_NEWLINE);

        return IPC_STATUS_OK;
    }

    printf( "index\t             [fun:line]                     index\t size\t data\t pid\t    %s", VTY_NEWLINE);

    for(i = 0; i < MEM_MAX_NUM; i++)
    {
        if (TRUE != astMemDebugInfo[i].ucValid)
        {
            continue;
        }

        printf( "%d: \t[%32s:%d]\t %d\t %d\t 0x%x\t %d    %s",
                i, astMemDebugInfo[i].szFunName, astMemDebugInfo[i].uiLine,
                astMemDebugInfo[i].uiIndex, astMemDebugInfo[i].uiSize,
                (UINT)astMemDebugInfo[i].pBuf, astMemDebugInfo[i].uiPid,VTY_NEWLINE);
    }

    return IPC_STATUS_OK;
}




